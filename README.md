# Tasks tracker

Works by accessing the `Plan` -> `Issue Boards`.

`Issues` are a list of tasks.
This is therefore a task tracker for a team working on a project or doing software development or engineering.

## Boards' Lists

`Development` *Board* has Six *Lists*:
- `Open` (aka `Backlog`): *tasks that are not yet assigned to a person or are blocked by something*
    - Next step: `ToDo`
    - Overcome by Event: move to `Closed`
- `Blocked`
    - Next step: `Open`, `Todo`, or `Doing`
    - Overcome by Event: move to `Closed`
- `ToDo`: *Next in Line, can be assigned, can be iterated on*
    - Next step: `Doing` 
    - Overcome by Event: move to `Closed`
- `Doing`: *(should) Have an **Assignee** and a **due date**, progress update in Daily Scrum and comment of task*
    - Next step: `Done`
    - Overcome by Event: move to `Closed`
- `Done`: *Completed by assignee, ready for review*
    - Next step: `Closed`
    - Overcome by Event: move to `Closed`
- `Closed`: *Confirmed completed to satisfaction of PM*
    - Only the PM is allowed to move task to this column

## Labels

Means of representing information about a given task.
If "displayed" on a board, they are also considered a "list".

Common ones:
- `Blocked` (in "Soft Red"): Task can not be iterated on. Internal or external blocker to be listed in `Comments` on the `Issue` tracker
- `Critical` (in "Strong Red"): Task is one of: a potential blocker for another person/task (ie "Critical Path"), needs to take priority over other tasks for a given assignee.

Level of Efforts:
- `LoE:Low`: Low
- `LoE:Med`: Medium
- `LoE:High`: High

Categories:
- `Admin`: Administrative support task (travel, purchase, ...)
- `Deliverable`: Contract Deliverable

## Milestones

An unit of internal deliverable. 
They offer to group a set of tasks together. 
Milestones should have due dates.

Progress on a milestone can be seen by its completion level.

## Issues

The core unit of *Task*:
- Any issue always start in `Open`. 
- When created, it is given a simple title. 
- Better if those are part of a `Milestone` to track overall completion. 
- `Labels` can be used to characterize it visually.
- After initial creation, additional information must be added (can be done by clicking on its title on the board)
    - add a _Description_ in order to explain what is expected (not at the blueprint level, but enough to allow for cooperators to help if needed)
    - _Comments_ can be used to track daily progress, information about the Task

### Issue comments

- If a Task refers to another task, add a comment with the task number to reference it. This can be done by using `#11` where 11 is the task number (autocomplete will offer options)
- If a Task referes to a different Milestone, add a comment with the Milestone information. This can be done by using `%milestone`  (autocomplete will offer options)

